var express = require('express');
var router = express.Router();
router.get('/', function(req, res, next) {
res.render('apply.ejs', { title: 'Express' });
});
// router.get('/login', function(req, res, next){
// res.render('login.ejs');
// });
// router.get('/apply', function(req, res, next){
// res.render('apply.ejs');
// });
// // router.post('/result', function(req, res){
// // res.render('result.ejs',{

// // user: req.body.user,
// // pass: req.body.pass
// // });
// // })
router.post('/result', function(req, res, next) {
  res.render('result.ejs', {
    id: req.body.id,
    prefix: req.body.prefix,
    fname: req.body.fname,
    lname: req.body.lname,
    nickname: req.body.nickname,
    birthday: req.body.birthday,
    province: req.body.province,
    county: req.body.county,
    district: req.body.district,
    home: req.body.home,
    road: req.body.road,
    post: req.body.post,
    telephone: req.body.telephone,
    fax: req.body.fax,
    smartphone: req.body.smartphone,
    email: req.body.email,
    prefix_en: req.body.prefix_en,
    fname_en: req.body.fname_en,
    lname_en: req.body.lname_en
  });
});



module.exports = router;

